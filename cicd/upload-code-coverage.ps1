$env:PY_VERSION=$(python -c "import sys; print('{}{}'.format(*sys.version_info))")
$env:SRC_REPORT_NAME="Py${PY_VERSION}-Windows-src"
$env:SRC_REPORT_FLAGS="windows,py${PY_VERSION},src"
$env:SRC_REPORT_PATH="artifacts/coverage-project.xml"
$env:TESTS_REPORT_NAME="Py${PY_VERSION}-Windows-tests"
$env:TESTS_REPORT_FLAGS="windows,py${PY_VERSION},tests"
$env:TESTS_REPORT_PATH="artifacts/coverage-tests.xml"

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls, [Net.SecurityProtocolType]::Tls11, [Net.SecurityProtocolType]::Tls12, [Net.SecurityProtocolType]::Ssl3
[Net.ServicePointManager]::SecurityProtocol = "Tls, Tls11, Tls12, Ssl3"

$ProgressPreference = 'SilentlyContinue'
Invoke-WebRequest -Uri https://keybase.io/codecovsecurity/pgp_keys.asc -OutFile codecov.asc
gpg.exe --import codecov.asc

Invoke-WebRequest -Uri https://uploader.codecov.io/latest/windows/codecov.exe -Outfile codecov.exe
Invoke-WebRequest -Uri https://uploader.codecov.io/latest/windows/codecov.exe.SHA256SUM -Outfile codecov.exe.SHA256SUM
Invoke-WebRequest -Uri https://uploader.codecov.io/latest/windows/codecov.exe.SHA256SUM.sig -Outfile codecov.exe.SHA256SUM.sig

gpg.exe --verify codecov.exe.SHA256SUM.sig codecov.exe.SHA256SUM
If ($(Compare-Object -ReferenceObject  $(($(certUtil -hashfile codecov.exe SHA256)[1], "codecov.exe") -join "  ") -DifferenceObject $(Get-Content codecov.exe.SHA256SUM)).length -eq 0) {
    echo "SHASUM verified"
} Else {
    exit 0
}

If (Test-Path -Path $env:SRC_REPORT_PATH -PathType leaf) {
    If (-not(./codecov.exe -R Get-Location -n "$env:SRC_REPORT_NAME" -f "$env:SRC_REPORT_PATH" -F "$env:SRC_REPORT_FLAGS")) {
        exit 0
    }
}

If (Test-Path -Path $env:TESTS_REPORT_PATH -PathType leaf) {
    If (-not(./codecov.exe -R Get-Location -n "$env:TESTS_REPORT_NAME" -f "$env:TESTS_REPORT_PATH" -F "$env:TESTS_REPORT_FLAGS")) {
        exit 0
    }
}
